// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MusicMysteryBoxGameMode.generated.h"

UCLASS(minimalapi)
class AMusicMysteryBoxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMusicMysteryBoxGameMode();
};



