// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MusicMysteryBox : ModuleRules
{
	public MusicMysteryBox(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
