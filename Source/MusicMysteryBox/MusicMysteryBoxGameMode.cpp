// Copyright Epic Games, Inc. All Rights Reserved.

#include "MusicMysteryBoxGameMode.h"
#include "MusicMysteryBoxCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMusicMysteryBoxGameMode::AMusicMysteryBoxGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
